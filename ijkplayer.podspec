Pod::Spec.new do |s|
    s.name               = "ijkplayer"
    s.version            = "1.3.2"
    s.summary            = "Android/iOS video player based on FFmpeg n2.8, with MediaCodec, VideoToolbox support."
    s.homepage           = "https://git.oschina.net/arenacloud/ijkplayer-ios-sdk"
    s.author             = "ArenaCloud"
    s.license            = { :type => "LGPL", :file => "LICENSE" }
    s.platform           = :ios, "7.0"
    s.source             = { :git => 'ttps://git.oschina.net/arenacloud/ijkplayer-ios-sdk.git', :tag => 'v' << s.version.to_s }
    s.source_files       = "SDK/iOS/include/*.{h,m,c}"
    s.requires_arc       = true

    s.frameworks         = [ 'VideoToolbox', 'AudioToolbox', 'AVFoundation', 'CFNetwork', 'CoreMedia',
    'CoreVideo', 'OpenGLES', 'Foundation', 'CoreGraphics' ]
    s.libraries          = [ 'c++', 'z' ]

    s.subspec 'include' do |ss|
    ss.source_files        = "*.h"
    ss.vendored_library   = "libIJKMediaPlayer.a"
    end
end
